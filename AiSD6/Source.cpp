#include <iostream>
#include <string>
#include <vector>
#include <array>

std::vector<int> generator_tps(std::string &wzorzec)
{
	std::vector<int> t(wzorzec.length());

	for (int i = 1, j = 0; i < wzorzec.length();++i)
	{
		if (wzorzec[i] == wzorzec[j])
			t[i] = ++j;
		else if (wzorzec[i] != wzorzec[j] && j == 0)
		{
			t[i] = 0;
		}
		else if (wzorzec[i] != wzorzec[j] && j>0)
		{
			j = t[j-1];
			--i;
		}
	}
	return t;
}

void KMP(std::string &tekst, std::string &wzorzec, std::vector<int> &tps)
{
	int i = 0, j = 0;
	while (i + j < tekst.length())
	{
		if (wzorzec[i] == tekst[i + j])
		{
			if (i == (wzorzec.length() - 1))
			{
				std::cout << "Znaleziono" << "\n";
				return;
			}
			else
				++i;
		}
		else
		{
			if (i > 0)
			{
				j = j + i - tps[i - 1];
				i = tps[i - 1];
			}
			else
			{
				i = 0;
				++j;
			}
		}
	}
}

int main()
{
	std::string tekst, wzorzec;
	std::vector<int> tps;
	tekst = "ABCEABCDABCDABG";
	wzorzec = "ABCDABC";

	tps = generator_tps(wzorzec);

	KMP(tekst, wzorzec, tps);

	for (auto &element : tps)
		std::cout << element << " ";

	system("pause");
}
